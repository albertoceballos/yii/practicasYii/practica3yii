<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';


?>
<div class="container">
<div class="site-index">
    <div class="body-content">
        <div class="row col-md-6">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                <span class="glyphicon glyphicon-user glyphicon user-img"></span>Usuario</div>
                <div class="panel-body">
                    <?php 
                    
                    foreach($fechas as $fecha){
                        echo "<div class='fechas'>$fecha->fecha</div>";
                        foreach($mensajes as $valor){ 
                           if($fecha->fecha==$valor->fecha){ 
                        ?>
                            <div class="alert alert-success mensaje" role="alert"><?= $valor->mensaje ?></div>
                            <div class="cfix hora"><span class="badge"><?=$valor->hora ?></span></div>
                            <div class="cfix"></div>
                    <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
